import numpy as np
from floodsystem.stationdata import build_station_list
from haversine import haversine, Unit
from floodsystem.geo import stations_within_radius

# Build list of stations
stations = build_station_list()
centre = (52.2053, 0.1218)

print(stations_within_radius(stations, centre, 10))
