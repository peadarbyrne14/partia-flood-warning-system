import datetime
from floodsystem import plot
from floodsystem import stationdata
from floodsystem import utils
from floodsystem import datafetcher
from floodsystem.flood import stations_highest_rel_level
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from floodsystem import analysis
from Task2E import stations_levels



def run():
    """Requirements for Task 2F"""
    num_days=2
    num_stat=5
    #number of extra stations to gather data from incase some have no level data for asked number of days
    extra=3
    # Fetches list of stations
    stations = stationdata.build_station_list()
    # Updates the water levels
    stationdata.update_water_levels(stations)

    # Creates a list of tuples of MonitoringStations and their relative water level (if this is not None)
    stations_and_levels=stations_levels(stations)
    
    # Sorts list of tuples of MonitoringStation and relative water level by highest relative water level
    stations_by_level = utils.sorted_by_key(stations_and_levels, 1, True)

    # Creates a list of eight MonitoringStations with highest relative water levels as some stations may not have data for the most recents in which we are trying to plot.
    stations_highest_level = list()

    for i in range(num_stat+extra):
        stations_highest_level.append(stations_by_level[i][0])
    
    # Creates a list of tuples of MonitoringStation, datetime.datetimes, and corresponding water levels for stated number of days and stations
    stations_with_dates_and_levels = list()
    stations_polyfit_functions= list()

    for i in stations_highest_level:
        if len(stations_with_dates_and_levels) == num_stat:
            break
        else:
            dates, levels = datafetcher.fetch_measure_levels(i.measure_id,
                                                         dt=datetime.timedelta(days=num_days))
            if not levels:
                pass
            else:
                stations_with_dates_and_levels.append([i,dates, levels])

    #Check that there was enough stations collected with valid data for plotting
    if len(stations_with_dates_and_levels) < num_stat:
        print("The required number of {} stations to plot was not met due to some stations having no recent level data. Increase extra.".format(num_days))
        return

    #Plot the data for all the stations over the required period
    for i in stations_with_dates_and_levels:
        plot.plot_water_level_with_fit(i[0], i[1], i[2], 4)


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
    