from floodsystem.geo import rivers_by_station_number
from floodsystem.station import MonitoringStation 

mock_stations=[]
mock_stations.append(MonitoringStation(0,0, "station 1",0 ,0 , "river 1", "town 1"))
mock_stations.append(MonitoringStation(0,0, "station 2",0 ,0 , "river 2", "town 2"))
mock_stations.append(MonitoringStation(0,0, "station 3",0 ,0 , "river 1", "town 3"))
mock_stations.append(MonitoringStation(0,0, "station 4",0 ,0 , "river 4", "town 4"))
mock_stations.append(MonitoringStation(0,0, "station 5",0 ,0 , "river 1", "town 5"))
mock_stations.append(MonitoringStation(0,0, "station 6",0 ,0 , "river 6", "town 6"))
mock_stations.append(MonitoringStation(0,0, "station 7",0 ,0 , "river 7", "town 7"))
mock_stations.append(MonitoringStation(0,0, "station 8",0 ,0 , "river 7", "town 8"))
mock_stations.append(MonitoringStation(0,0, "station 9",0 ,0 , "river 9", "town 9"))
mock_stations.append(MonitoringStation(0,0, "station 10",0 ,0 , "river 9", "town 10"))

def test_rivers_by_station_number():
    assert len(rivers_by_station_number(mock_stations,2))==3

print(rivers_by_station_number(mock_stations,2))

#[("river 1",3),("river 7",2),("river 9",2)]