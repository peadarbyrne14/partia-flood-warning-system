#taskE
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

# Build list of stations
stations = build_station_list()

#Demonstration program
print(rivers_by_station_number(stations,5))
print(rivers_by_station_number(stations,9))


