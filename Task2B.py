from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels

tol = 0.8

def run():
    # print the name of each station at which the current relative level is over 0.8,
    # with the relative level alongside the name
    stations = build_station_list()
    update_water_levels(stations)
    level_over_tol = stations_level_over_threshold(stations, tol)
    for i in level_over_tol:
        print(i)

run()