# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def typical_range_consistent(self):
        #add method to MonitoringStation that checks typical high/low range data for consistency
        #return True/False depending on consistency
        consistent = True
        ranges = self.typical_range
        try:
            lower = ranges[0]
            upper = ranges[1]
            if type(lower) != float:
                consistent = False
            elif type(upper) != float:
                consistent = False
            elif lower > upper:
                consistent = False
        except TypeError:
            consistent = False
        return consistent

    def relative_water_level(self):
        # returns the latest water levels as a fraction of the typical range
        relative_level = 0
        latest = self.latest_level
        ranges = self.typical_range
        consistent = self.typical_range_consistent()
        if consistent == True and type(latest) == float:
            lower = ranges[0]
            upper = ranges[1]
            relative_level = abs((latest - lower)/(upper - lower))
        else: 
            relative_level = None
        return relative_level


def inconsistent_typical_range_stations(stations):
    #given a list of stations, return a list of stations wtih inconsistent data
    inconsistent_stations = []
    for station in stations:
        obj = station.typical_range_consistent()
        if obj == False:
            inconsistent_stations.append(station.name)
    return inconsistent_stations