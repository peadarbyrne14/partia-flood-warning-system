from floodsystem.geo import stations_by_distance
from floodsystem.station import MonitoringStation
from haversine import haversine, Unit

def test_station_distance():

    p = (52.2053, 0.1218)

    S1 = MonitoringStation("1", "1", "1", (52.2, 0.12), (0, 1), "1", "1")
    S2 = MonitoringStation("2", "2", "2", (52.0, 0.1), (0, 1), "2", "2")
    mock_list = [S1, S2]
    sorted_list = stations_by_distance(mock_list, p)
    
    assert sorted_list == [['1', 0.6019647853993659], ['2', 22.8768569457285]]
