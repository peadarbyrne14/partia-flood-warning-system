import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import numpy as np
from floodsystem import analysis
from floodsystem.plot import plot_water_levels
from floodsystem.plot import plot_water_level_with_fit

def test_plot_water_levels():
    plot_water_levels.has_been_called = True

plot_water_levels.has_been_called = False

def test_plot_water_level_with_fit():
    plot_water_level_with_fit.has_been_called = True

plot_water_level_with_fit.has_been_called = False