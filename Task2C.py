# Provide a program file Task2C.py that prints the names of the 10 stations at which
#  the current relative level is highest, with the relative level beside each station name

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
N = 10

def run():
    stations = build_station_list()
    update_water_levels(stations)
    highest_level= stations_highest_rel_level(stations, N)
    for i in highest_level:
        print(i)

run()