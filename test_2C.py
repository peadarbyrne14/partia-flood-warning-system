from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def test_highest_relative_level():
    stations = build_station_list()
    update_water_levels(stations)
    highest_level= stations_highest_rel_level(stations, 12)
    
    assert len(highest_level) == 12
    for i in range(len(highest_level)):
        assert type(highest_level[i][1]) == float or int