import numpy as np
import matplotlib
import matplotlib.pyplot as plt



def polyfit(dates, levels, p):

    x = matplotlib.dates.date2num(dates)
    y = levels

    d0=x[0]
    # Find coefficients of best-fit polynomial f(x) of degree p
    p_coeff = np.polyfit(x-d0, y, p)

    # Convert coefficient into a polynomial
    poly = np.poly1d(p_coeff)
    function_info=(poly, d0)
    return function_info

