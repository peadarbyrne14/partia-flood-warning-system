from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
stations = build_station_list()

list = inconsistent_typical_range_stations(stations)
sorted_list = sorted(list)
print(sorted_list)