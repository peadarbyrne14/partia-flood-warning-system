# rank stations on risk:
# severe- relative level greater than 5
# high- more than 2
# moderate- more than 1
# low, in or below typical range
# plot results on a map (severe=red, high=orange, moderate=yellow, low=green)

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np

from floodsystem.flood import stations_highest_rel_level
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels

stations = build_station_list()
update_water_levels(stations)
severe_x, severe_y, high_x, high_y, moderate_x, moderate_y, low_x, low_y = [],[],[],[],[],[],[],[]

for station in stations:
        if station.relative_water_level() is not None and station.relative_water_level() > 5:
                severe_x.append(station.coord[1])
                severe_y.append(station.coord[0])
        elif station.relative_water_level() is not None and station.relative_water_level() > 2:
                high_x.append(station.coord[1])
                high_y.append(station.coord[0])
        elif station.relative_water_level() is not None and station.relative_water_level() > 1:
                moderate_x.append(station.coord[1])
                moderate_y.append(station.coord[0])
        elif station.relative_water_level() is not None and station.relative_water_level() <= 1:
                low_x.append(station.coord[1])
                low_y.append(station.coord[0])
        else:
                pass

# create map of England
m = Basemap(projection='mill', llcrnrlat = 49, llcrnrlon = -8, urcrnrlat = 59, urcrnrlon = 3.5, resolution = 'l')

m.drawcoastlines()
m.drawcountries(linewidth=2)
m.bluemarble()

Lx, Ly = m(low_x, low_y)
plt.plot(Lx, Ly, '.', markersize=0.1, color='limegreen')

Mx, My = m(moderate_x, moderate_y)
plt.plot(Mx, My, '.', markersize=0.8, color='yellow')

Hx, Hy = m(high_x, high_y)
plt.plot(Hx, Hy, '.', markersize=3, color='orange')

Sx, Sy = m(severe_x, severe_y)
plt.plot(Sx, Sy, '.', markersize=6, color='firebrick')

plt.show()
