import datetime
from floodsystem import plot
from floodsystem import stationdata
from floodsystem import utils
from floodsystem import datafetcher
from floodsystem.flood import stations_highest_rel_level


def stations_levels(stations):
    stations_and_levels = list()
    for i in stations:
        if i.relative_water_level() != None:
            stations_and_levels.append((i, i.relative_water_level()))
    return stations_and_levels

def run():
    """Requirements for Task 2E"""

    # Fetches list of stations
    stations = stationdata.build_station_list()
    # Updates the water levels
    stationdata.update_water_levels(stations)

    # Creates a list of tuples of MonitoringStations and their relative water level (if this is not None)
    stations_and_levels=stations_levels(stations)
    
    # Sorts list of tuples of MonitoringStation and relative water level by highest relative water level
    stations_by_level = utils.sorted_by_key(stations_and_levels, 1, True)

    # Creates a list of five MonitoringStations with highest relative water levels
    five_stations_highest_level = list()

    for i in range(5):
        five_stations_highest_level.append(stations_by_level[i][0])

    # Creates a list of tuples of MonitoringStation, datetime.datetimes, and corresponding water levels
    # For five_stations_higheset_level over the past 10 days
    stations_with_dates_and_levels = list()

    for i in five_stations_highest_level:
        dates, levels = datafetcher.fetch_measure_levels(i.measure_id,
                                                         dt=datetime.timedelta(days=10))
        stations_with_dates_and_levels.append([i, dates, levels])
    
    # Creates plots for each of five MonitoringStations levels over the past 10 days
    for i in stations_with_dates_and_levels:
        plot.plot_water_levels(i[0], i[1], i[2])


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()




