from .station import MonitoringStation
from .stationdata import build_station_list, update_water_levels
from . import utils

stations = build_station_list()

"2B"

def stations_level_over_threshold(stations, tol):
    # return a list of tuples containing station names with relative water 
    # level over tol and relative water level
    stations_levels = []
    for station in stations:
        if station.relative_water_level() is not None and station.relative_water_level() > tol:
            stations_levels.append((station.name, station.relative_water_level()))
    return utils.sorted_by_key(stations_levels, 1, reverse=True)

"2C"

def stations_highest_rel_level(stations, N):
    stations_levels = []
    for station in stations:
        if station.relative_water_level() is not None:
            stations_levels.append((station.name, station.relative_water_level()))
    stations_sorted = utils.sorted_by_key(stations_levels, 1, reverse=True)
    stations_high = []
    for i in range(N):
        stations_high.append(stations_sorted[i])
    return stations_high