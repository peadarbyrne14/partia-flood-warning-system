from floodsystem.station import MonitoringStation
from haversine import haversine, Unit

def test_inconsistent_data():

    S1 = MonitoringStation("1", "1", "1", (52.0, 0.1), (1.0, 2.5), "1", "1")
    S2 = MonitoringStation("2", "2", "2", (52.0, 0.1), ("lower", "upper"), "2", "2")
    S3 = MonitoringStation("3", "3", "3", (52.0, 0.1), (3.0, 0.5), "3", "3")
    
    assert MonitoringStation.typical_range_consistent(S1) == True
    assert MonitoringStation.typical_range_consistent(S2) == False
    assert MonitoringStation.typical_range_consistent(S3) == False