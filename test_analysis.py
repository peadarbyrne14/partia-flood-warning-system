import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from floodsystem.analysis import polyfit
import datetime

def test_polyfit():
    polyfit.has_been_called = True

polyfit.has_been_called = False




a = datetime.datetime.today()
numdays = 10
dateList = []
for x in range (0, numdays):
    dateList.append(a - datetime.timedelta(days = x))

x = dateList
y = [0.1, 0.09, 0.23, 0.34, 0.78, 0.74, 0.43, 0.31, 0.01, -0.05]
p=3

def test_polyfit():
    assert len(polyfit(x,y,p)) == 2
    assert type(polyfit(x,y,p)) == tuple
    assert len(polyfit(x,y,p)[0]) == p