# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from floodsystem.utils import sorted_by_key
import numpy as np
from floodsystem.stationdata import build_station_list
from haversine import haversine, Unit

"1B"

def stations_by_distance(stations, p):
    station_list = []
    #loop over stations
    for i in range(len(stations)):
        #append tuple to main list
        station_list.append([(stations[i].name), haversine(stations[i].coord, p)])
        station_list.sort(key=lambda x: x[1])
    return station_list

"1C"

def stations_within_radius(stations, centre, r):
    radius_list = []
    for station in stations:
        distance = haversine(centre, station.coord)
        if distance <= r:
            radius_list.append(station.name)
    return sorted(radius_list)

"1D"


def rivers_with_station(stations):
    "A function that creates a set of river names"
    rivers=[]
    for i in range(len(stations)):
        #append data from stations list to form a list of tuples of river name to station
        rivers.append((stations[i].river))
    return set(rivers)

def stations_by_river(stations):
    "Create a dictionary wtih river name as key to search for a station"
    river_station_dictionary={}
    for i in range(len(stations)):
        #iterating through the rivers. If it already exsists it is added to a list of stations associated with that river
        if stations[i].river in river_station_dictionary.keys():
            river_station_dictionary[stations[i].river].append(stations[i].name)
        #Creates a new dictionary entry with the river as the key and adds the station to list for that key
        else:
            river_station_dictionary[stations[i].river]=[]
            river_station_dictionary[stations[i].river].append(stations[i].name) 
    return river_station_dictionary


"1E"
def rivers_by_station_number(stations,N):
    "Returns a list of N rivers with the greatest number of monitoring stations"
    #dictionary of rivers with associated stations
    dictionary=stations_by_river(stations)
    #converting a set of all the rivers to a list
    rivers=sorted(rivers_with_station(stations))
    #Creating list of tuple pairs of river name to number of stations and sorting it by station number
    river_number=[] 
    for i in range(len(rivers)): 
        river_number.append((rivers[i],len(dictionary[rivers[i]])))    
    river_number.sort(key=lambda tup: tup[1],reverse=True)
    #shortening list to the Nth amount of rivers with the most stations

    Nth_most_stations=[]
    for i in range(N):
        Nth_most_stations.append(river_number[i])

    lastnumber=Nth_most_stations[-1][1]
    #length=len(Nth_most_stations)

    if lastnumber > river_number[N][1]:
        return Nth_most_stations
    else: 
        safety_count=0
        while lastnumber == river_number[N][1]:
            Nth_most_stations.append(river_number[N])
            N=N+1
            safety_count+=1
            if safety_count > 3000:
                print("List became too large")
                break
            
            
        return Nth_most_stations

