# test all return values have a relative level over 0.8

from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels

# since stations_level function uses the relative_water_level function, this tests for the functionality of both functions
def test_stations_level_over_threshold():
    stations = build_station_list()
    update_water_levels(stations)
    level_over_tol = stations_level_over_threshold(stations, 0.9)
    for i in range(len(level_over_tol)):
        assert level_over_tol[i][1] > 0.9