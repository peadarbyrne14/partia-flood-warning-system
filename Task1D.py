#task1D
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
# Build list of stations
stations = build_station_list()


# def rivers_with_station(stations):
#     "A function that creates a set of river names"
#     rivers=[]
#     for i in range(len(stations)):
#         #append data from stations list to form a list of tuples of river name to station
#         rivers.append((stations[i].river))
#     return set(rivers)

# def stations_by_river(stations):
#     "Create a dictionary wtih river name as key to search for a station"
#     river_station_dictionary={}
#     for i in range(len(stations)):
#         #iterating through the rivers. If it already exsists it is added to a list of stations associated with that river
#         if stations[i].river in river_station_dictionary.keys():
#             river_station_dictionary[stations[i].river].append(stations[i].name)
#         #Creates a new dictionary entry with the river as the key and adds the station to list for that key
#         else:
#             river_station_dictionary[stations[i].river]=[]
#             river_station_dictionary[stations[i].river].append(stations[i].name) 
#     return river_station_dictionary


#Demonstration program

#Part 1
print("The number of rivers with at least one stations is {}".format(len(rivers_with_station(stations))))

#Part2
#creating dictionary of rivers to stations
river_station_dictionary=stations_by_river(stations)
    
def station_search(river):
    "Gets the list of stations for the input river and sorts the list"
    return sorted(river_station_dictionary.get(river))
    
print("--------")
print("The stations on the River Aire are {}".format(station_search("River Aire")))
print("--------")
print("The stations on the River Cam are {}".format(station_search("River Cam")))
print("--------")
print("The stations on the River Thames are {}".format(station_search("River Thames")))
