"""
This module contains functions for plotting data from stations """

import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import numpy as np
from floodsystem import analysis
#from analysis import polyfit


def plot_water_levels(station, dates, levels):
    """Plots water level data stored in 'levels' for 'station' against time from 'dates'"""
    
    # Creates plot
    plt.plot(dates, levels)

    # Adds horizontal lines of low and high values of typical_range, provided these are consistent
    if station.typical_range_consistent():
        plt.hlines(station.typical_range, dates[0], dates[-1])

    # Adds axis titles
    plt.xlabel("date")
    plt.ylabel("water level (m)")

    # Rotates date labels 45 degrees
    plt.xticks(rotation=45)

    # Adds the name of station title to plot
    plt.title(station.name)

    # Makes sure plot does not cut off date labels
    plt.tight_layout()

    # Displays plot
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    """Plots water level data stored in 'levels' for 'station' against time from 'dates'"""

    x = matplotlib.dates.date2num(dates)
    y = levels
    
    #Plot original data points
    plt.plot(x, y,'.')

    # Adds axis titles
    plt.xlabel("date")
    plt.ylabel("water level (m)")

    # Rotates date labels 45 degrees
    plt.xticks(rotation=45)

    # Adds the name of station title to plot
    plt.title(station.name)

    # # Adds horizontal lines of low and high values of typical_range, provided these are consistent
    if station.typical_range_consistent():
        plt.hlines(station.typical_range, dates[0], dates[-1])

    #Calculate the polynomial coefficients
    poly, d0 = analysis.polyfit(dates, levels, p)

    # Plot polynomial fit at 30 points along interval
    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1, poly(x1 - x[0]))

    # Makes sure plot does not cut off date labels
    plt.tight_layout()

    # Displays plot
    plt.show()
